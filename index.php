<html>
<head>
    <title>IFMS - Ramais Telefônicos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/touch_icon.png">

    <!-- Plone CSS - only for preview, will be removed in rules.xml -->
    <link media="screen" href="css/plone.css" type="text/css" rel="stylesheet" id="plone-css">

    <!-- Theme CSS - only for preview, will be removed in rules and added in CSS registry -->
    <link media="all" href="css/main.css" type="text/css" rel="stylesheet" id="main-css">
    <link media="all" href="css/style.css" type="text/css" rel="stylesheet">
</head>

<body>
    <div id="barra-brasil" style="background:#7F7F7F; height: 20px; padding:0 0 0 10px;display:block;"> 
        <ul id="menu-barra-temp" style="list-style:none;">
          <li style="display:inline; float:left;padding-right:10px; margin-right:10px; border-right:1px solid #EDEDED"><a href="http://brasil.gov.br" style="font-family:sans,sans-serif; text-decoration:none; color:white;">Portal do Governo Brasileiro</a></li> 
          <li><a style="font-family:sans,sans-serif; text-decoration:none; color:white;" href="http://epwg.governoeletronico.gov.br/barra/atualize.html">Atualize sua Barra de Governo</a></li>
        </ul>
    </div>  
    <div id="wrapper">
    <div id="header" role="banner">
      <div>
        <div id="logo">
          <a id="portal-logo" title="Ramais Telefônicos do Instituto Federal de Mato Grosso do Sul." href="http://10.1.0.8/ramais">
            <span id="portal-title-1">Instituto Federal de Mato Grosso do Sul</span>
            <h1 id="portal-title" class="corto">Ramais Telefônicos</h1>
            <span id="portal-description"></span>
          </a>
        </div>
      </div>
    </div>
    <div id="sobre">
      <ul>
        <li class="portalservicos-item"><a href="./duvidas" title="Dúvidas?">Dúvidas</a></li>
        <li class="portalservicos-item last-item"><a href="./contato" title="Contato">Contato</a></li>
        </ul>
    </div>
    <!-- content -->
    <div id="main" role="main">
        <!--
        <div id="portal-features">
            <div id="featured-content">
                <div class="row">
                    <div class="cell width-16 position-0 " data-panel="">
                        <div class="tile" data-tile="@@em_destaque/d0c60d47c0f443bc9e14337136595016" id="d0c60d47c0f443bc9e14337136595016">
                            <div id="em-destaque">
                                <ul class="sortable-tile cover-list-tile">
                                    <li id="em-destaque-titulo">Em destaque</li>
                                    <li class="">
                                      <a href="http://www.ifms.edu.br" title="www.ifms.edu.br">Site do IFMS</a>
                                  </li>
                                  <li class="last-item">
                                      <a href="http://servidor.ifms.edu.br" title="Página do Servidor">Página do Servidor</a>
                                  </li>
                                </ul>
                                <div class="visualClear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        -->
        <div id="plone-content">

        <!-- Demo Plone Content -->

        <div id="portal-columns" class="row">

            <!-- Column 1 -->
            <div id="navigation">
                <a name="anavigation" id="anavigation"></a>
                <span class="menuTrigger">Menu</span>
                <div id="portal-column-one" class="cell width-1:4 position-0">
                    <div class="portletWrapper">
                         <dl class="portlet portletNavigationTree">
                            <dd class="portletItem lastItem">
                                <ul class="navTree navTreeLevel0">
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="http://www.ifms.edu.br" title="Site do IFMS" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Site do IFMS</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish last-item">
                                        <a href="http://servidor.ifms.edu.br" title="Página do Servidor" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Página do Servidor</span>
                                        </a>
                                    </li>
                                </ul>
                            </dd>
                        </dl>
                        <dl class="portlet portletNavigationTree">
                            <dt class="portletHeader">
                                <span class="portletTopLeft"></span>
                                Campi
                                <span class="portletTopRight"></span>
                            </dt>
                            <dd class="portletItem lastItem">
                                <ul class="navTree navTreeLevel0">
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./aquidauana" title="Campus Aquidauana" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Aquidauana</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./campo-grande" title="Campus Campo Grande" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Campo Grande</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./corumba" title="Campus Corumbá" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Corumbá</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./coxim" title="Campus Coxim" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Coxim</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./dourados" title="Campus Dourados" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Dourados</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./jardim" title="Campus Jardim" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Jardim</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./navirai" title="Campus Naviraí" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Naviraí</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./nova-andradina" title="Campus Nova Andradina" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Nova Andradina</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./ponta-pora" title="Campus Ponta Porã" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Ponta Porã</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish">
                                        <a href="./tres-lagoas" title="Campus Três Lagoas" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Três Lagoas</span>
                                        </a>
                                    </li>
                                    <li class="navTreeItem visualNoMarker navTreeFolderish last-item">
                                        <a href="./reitoria" title="Reitoria" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Reitoria</span>
                                        </a>
                                    </li>
                                </ul>
                                <span class="portletBottomLeft"></span>
                                <span class="portletBottomRight"></span>
                            </dd>
                        </dl>
                        <dl class="portlet portletNavigationTree">
                             <dt class="portletHeader">
                                <span class="portletTopLeft"></span>
                                Videoconferência
                                <span class="portletTopRight"></span>
                            </dt>
                            <dd class="portletItem lastItem">
                                <ul class="navTree navTreeLevel0">
                                    <li class="navTreeItem visualNoMarker navTreeFolderish last-item">
                                        <a href="./videoconferencia" title="Ramais de videoconferência" class="state-published navTreeFolderish contenttype-folder">
                                            <span>Ramais</span>
                                        </a>
                                    </li>
                                </ul>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>

            <!-- Conteudo -->
            <div id="portal-column-content" class="cell width-1:1 position-1:4">
                <?php
                if (isset($_GET['page']))
                {
                    switch ($_GET['page']){
                        case 'aquidauana':
                            $title = 'Campus Aquidauana';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=1658624779&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;
                        case 'campo-grande':
                            $title = 'Campus Campo Grande';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=1267604562&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;           
                        case 'corumba':
                            $title = 'Campus Corumbá';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=477290194&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;           
                        case 'coxim':
                            $title = 'Campus Coxim';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=225848695&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break; 
                        case 'dourados':
                            $title = 'Campus Dourados';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=973262086&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;   
                        case 'nova-andradina':
                            $title = 'Campus Nova Andradina';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=1677299660&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;          
                        case 'jardim':
                            $title = 'Campus Jardim';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=1858188913&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break; 
                        case 'navirai':
                            $title = 'Campus Naviraí';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=443350924&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;
                        case 'tres-lagoas':
                            $title = 'Campus Três Lagoas';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=1757900285&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break; 
                        case 'ponta-pora':
                            $title = 'Campus Ponta Porã';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=0&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;   
                        case 'duvidas':
                            $title = 'Dúvidas';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=248953189&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            break;  
                        case 'contato':
                            $title = 'Contato';
                            $link = 'page_contato.php';
                            break;    
                        case 'videoconferencia':
                            $title = 'Ramais de Videoconferência';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=2125600151&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            $height = '1200px';
                            break;    
                        default:
                            $title = 'Reitoria';
                            $link = 'https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=641280296&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false';
                            $height = '2000px';
                            break;               
                    }   
                }else
			        $title = 'Reitoria';
			
                ?>
                <a name="acontent" id="acontent"></a>
                <div id="main-content" class="">
                    <div id="content">
                        <h1 id="parent-fieldname-title" class="documentFirstHeading"><?php echo $title; ?></h1>
                        <div id="content-core">
                            <?php
                            if (!isset($height))
                                $height = '800px';

                            if (isset($link))
                                echo '<iframe src="' . $link . '" width="750px" height="' . $height . '" frameborder="0" scrolling="no" style="border: 0"></iframe>';
                            else
                                echo '<iframe src="https://docs.google.com/spreadsheets/d/148nqQwwbLzPhJN8PoJJczq_QRC_hQoe_UYBiZHnm1WU/pubhtml?gid=641280296&amp;single=true&amp;widget=false&amp;chrome=false&amp;headers=false" width="750px" height="1500px" frameborder="0" scrolling="no" style="border: 0"></iframe>';
                            ?>
                        </div>
                    </div>
                </div>
                <div id="viewlet-below-content"></div>
            </div>
        </div>

        <!-- /Demo Plone Content -->

    </div>

    <div class="clear"></div>
    <div id="voltar-topo">
        <a href="#wrapper">Voltar para o topo</a>
    </div>

</div>

<!-- Footer -->
<div id="footer" role="contentinfo">
    <div id="footer-brasil" class="footer-logos">
        <div>
            <a href="http://www.acessoainformacao.gov.br/" class="logo-acesso">
                <img src="img/acesso-a-infornacao.png" alt="Acesso a Informa&ccedil;&atilde;o" />
            </a>
            <a href="http://www.brasil.gov.br/" class="logo-brasil">
                <img src="img/brasil_patria.png" alt="Brasil - Governo Federal" />
            </a>
        </div>
    </div>
</div>
<div id="extra-footer">
    <p>
    Direitos reservados a <a href="https://github.com/plonegovbr/brasil.gov.temas">plonegovbr/brasil.gov.temas</a>.
    </p>
</div>
<!-- /Footer-->
    <script defer="defer" src="//barra.brasil.gov.br/barra.js" type="text/javascript"></script>
</body>
</html>
