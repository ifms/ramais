<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" dir="ltr">
<head>
    <title>IFMS - Ramais Telefônicos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/touch_icon.png">

    <!-- Plone CSS - only for preview, will be removed in rules.xml -->
    <link media="screen" href="css/plone.css" type="text/css" rel="stylesheet" id="plone-css">

    <!-- Theme CSS - only for preview, will be removed in rules and added in CSS registry -->
    <link media="all" href="css/main.css" type="text/css" rel="stylesheet" id="main-css">
    <link media="all" href="css/style.css" type="text/css" rel="stylesheet">
</head>

<body>
	<div id="main" role="main">
		<p>Ainda tem dúvidas de como usar o serviço ou percebeu que o ramal que você utiliza está desatualizado? É só entrar em contato com a Dirti. O e-mail é sd@ifms.edu.br.</p>
	</div>
</body>
</html>